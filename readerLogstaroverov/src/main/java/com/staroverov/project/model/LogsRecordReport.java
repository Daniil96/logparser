package com.staroverov.project.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LogsRecordReport {

    private int thereWereLines;
    private int rowsProcessed;
    private int rowsSkipped;
    private int rowsInsertedIntoDatabase;
}
