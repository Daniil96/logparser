package com.staroverov.project.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class CellLog {

    public static final String TABLE_NAME = "cell_log";
    public static final String ID_COLUMN = "id";
    public static final String DATE = "date";
    public static final String LOG_LVL = "log_lvl";
    public static final String THREAD = "thread";
    public static final String LOG_MESSAGE = "log_message";

    private Timestamp data;
    private String thread;
    private String logLvl;
    private String logMessage;
}
