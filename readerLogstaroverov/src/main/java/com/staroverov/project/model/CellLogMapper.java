package com.staroverov.project.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CellLogMapper implements RowMapper {
    @Override
    public Object mapRow(ResultSet resultSet, int i) throws SQLException {
        CellLog cellLog = new CellLog();
        cellLog.setData(resultSet.getTimestamp(CellLog.DATE));
        cellLog.setThread(resultSet.getString(CellLog.THREAD));
        cellLog.setLogLvl(resultSet.getString(CellLog.LOG_LVL));
        cellLog.setLogMessage(resultSet.getString(CellLog.LOG_MESSAGE));
        return cellLog;
    }
}
