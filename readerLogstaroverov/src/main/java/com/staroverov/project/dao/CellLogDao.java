package com.staroverov.project.dao;

import com.staroverov.project.model.CellLog;

import java.util.List;


public interface CellLogDao {
    String SQL_FIND_ALL = "select * from " + CellLog.TABLE_NAME;
    String SQL_FIND_BY_ID = SQL_FIND_ALL + " where " + CellLog.ID_COLUMN + " = ?";
    String SQL_INSERT = "insert into " + CellLog.TABLE_NAME + " (" +
            CellLog.DATE + ", " +
            CellLog.LOG_LVL + ", " +
            CellLog.THREAD + ", " +
            CellLog.LOG_MESSAGE + ") values (?, ?, ?, ?)";

    String SQL_DELETE = "delete from " + CellLog.TABLE_NAME + " where " + CellLog.ID_COLUMN + " = ?";

    List findAll();

    CellLog findById(Long id);

    void insert(CellLog cellLog);

    void delete(Long id);
}
