package com.staroverov.project.dao.imlp;

import com.staroverov.project.dao.CellLogDao;
import com.staroverov.project.model.CellLog;
import com.staroverov.project.util.dataBase.DataSource;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class CellLogDaoImpl implements CellLogDao {

    @Autowired
    private DataSource dataSource;

    @Override
    public List<CellLog> findAll() {
        List<CellLog> result = new ArrayList<>();

        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {

                CellLog item = CellLog.builder()
                        .data(rs.getTimestamp(CellLog.DATE))
                        .logLvl(rs.getString(CellLog.LOG_LVL))
                        .thread(rs.getString(CellLog.THREAD))
                        .logMessage(rs.getString(CellLog.LOG_MESSAGE))
                        .build();

                result.add(item);
            }
        } catch (SQLException e) {
            System.out.println("Не удаётся поключиться к базе данных");
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @Override
    public CellLog findById(Long id) {
        CellLog cellLog = null;
        Connection connection = null;

        try {
            connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID);
            statement.setLong(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {

                cellLog = CellLog.builder()
                        .data(rs.getTimestamp(CellLog.DATE))
                        .logLvl(rs.getString(CellLog.LOG_LVL))
                        .thread(rs.getString(CellLog.THREAD))
                        .logMessage(rs.getString(CellLog.LOG_MESSAGE))
                        .build();
            }
        } catch (SQLException e) {
            e.setNextException(new SQLException("Проверка задачи текста ексепшена"));
            System.out.println("Не удаётся поключиться к базе данных");
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return cellLog;
    }

    @Override
    public void insert(CellLog cellLog) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT);
            statement.setTimestamp(1, cellLog.getData());
            statement.setString(2, cellLog.getLogLvl());
            statement.setString(3, cellLog.getThread());
            statement.setString(4, cellLog.getLogMessage());
            statement.execute();
        } catch (SQLException e) {
            System.out.println("Не удаётся поключиться к базе данных");
            //e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void delete(Long id) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE);
            statement.setLong(1, id);
            statement.execute();

            System.out.println("Лог с id" + id + "удалён");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
    }
}

