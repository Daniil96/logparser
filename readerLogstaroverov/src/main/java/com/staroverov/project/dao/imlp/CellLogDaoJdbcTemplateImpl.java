package com.staroverov.project.dao.imlp;

import com.staroverov.project.dao.CellLogDao;
import com.staroverov.project.model.CellLog;
import com.staroverov.project.model.CellLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CellLogDaoJdbcTemplateImpl implements CellLogDao {


    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DriverManagerDataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }



    @Override
    public List findAll() {
        return jdbcTemplate.query(SQL_FIND_ALL, new CellLogMapper());
    }

    @Override
    public CellLog findById(Long id) {
        return (CellLog) jdbcTemplate.queryForObject(SQL_FIND_BY_ID,new Object[]{id}, new CellLogMapper() );
    }

    @Override
    public void insert(CellLog cellLog) {
        jdbcTemplate.update(SQL_INSERT, cellLog.getData(),
                cellLog.getLogLvl(),
                cellLog.getThread(),
                cellLog.getLogMessage());
    }

    @Override
    public void delete(Long id) {
        jdbcTemplate.update(SQL_DELETE, id);
    }
}
