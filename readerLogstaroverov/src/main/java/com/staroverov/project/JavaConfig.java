package com.staroverov.project;

import com.staroverov.project.dao.imlp.CellLogDaoJdbcTemplateImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JavaConfig {

    @Bean
    public CellLogDaoJdbcTemplateImpl cellLogDaoJdbcTemplateImpl(){
        return new CellLogDaoJdbcTemplateImpl();
    }
}
