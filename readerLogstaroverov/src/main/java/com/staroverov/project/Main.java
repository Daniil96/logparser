package com.staroverov.project;

import com.staroverov.project.dao.CellLogDao;
import com.staroverov.project.dao.imlp.CellLogDaoJdbcTemplateImpl;
import com.staroverov.project.model.CellLog;
import com.staroverov.project.model.LogsRecordReport;
import com.staroverov.project.util.library.FileLogWorker;
import com.staroverov.project.util.library.LogParser;
import com.staroverov.project.util.library.WorkWithReportOfLogs;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Scanner;


public class Main {

    static ApplicationContext context = new AnnotationConfigApplicationContext(ConfigurationSpring.class);

    private static FileLogWorker fileLogWorker = context.getBean("fileLogWorker", FileLogWorker.class);
    private static LogParser logParser = context.getBean("logParser", LogParser.class);
    //private static CellLogDao cellLogDao = context.getBean("cellLogDaoImpl", CellLogDaoImpl.class);
    private static CellLogDao cellLogDao = context.getBean(CellLogDaoJdbcTemplateImpl.class);
    private static WorkWithReportOfLogs workWithReportOfLogs = context.getBean(WorkWithReportOfLogs.class);


    public static void main(String[] args) {

        boolean formIsActive = true;

        while (formIsActive) {
            try {

                System.out.println("Начало");
                System.out.println("Введите путь к файлу");

                Scanner scanner = new Scanner(System.in);
                StringBuilder logsFile = new StringBuilder();

                try {
                    logsFile = fileLogWorker.readLogFile(scanner.nextLine());
                    //logsFile = fileLogWorker.readLogFile("C:\\Users\\dandk\\IdeaProjects\\logparser\\temp\\logsTest.log");
                    scanner.close();

                    List<CellLog> logs = logParser.parseLogFIleToList(logsFile);

                    int coutNodeInDB = 0;
                    for (CellLog item : logs) {
                        System.out.println(item.toString());
                        cellLogDao.insert(item);
                        coutNodeInDB++;
                    }


                    LogsRecordReport logsRecordReport = workWithReportOfLogs.toMakeReport(logs, logsFile);
                    logsRecordReport.setRowsInsertedIntoDatabase(coutNodeInDB);

                    System.out.println(logsRecordReport.toString());

                    System.out.println("Конец");

                    formIsActive = false;

                } catch (IOException e) {
                    System.out.println(e.getMessage());// e.printStackTrace();
                }

            }catch (Exception ex){
                System.out.println(ex.getMessage());
                formIsActive = false;
            }
        }
    }
}
