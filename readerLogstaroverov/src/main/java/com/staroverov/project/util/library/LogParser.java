package com.staroverov.project.util.library;

import com.staroverov.project.model.CellLog;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
@NoArgsConstructor
public class LogParser {

    private List<CellLog> listCellLog;

    private static final int DATE_GROUP_ID_IN_PATTERN = 1;
    private static final int LOG_LVL_GROUP_ID_IN_PATTERN = 2;
    private static final int THREAD_GROUP_ID_IN_PATTERN = 3;
    private static final int LOG_MESSAGE_GROUP_ID_IN_PATTERN = 4;

    private Pattern normalizeLogPattern = Pattern.compile("(\\S{10}\\s\\S{9,12})\\s\\[(.*)\\]\\s(\\S*)\\s{17}(.*)\r\n");


    public List<CellLog> parseLogFIleToList(StringBuilder stringBuffer) {

        listCellLog = new ArrayList<>();

        //Найдем все подходящие строки
        List<String> validationLog = getValidationLogStrings(stringBuffer);

        StringBuilder thread = new StringBuilder();
        StringBuilder logLvl = new StringBuilder();
        StringBuilder logMessage = new StringBuilder();

        for (String log : validationLog) {

            Timestamp date = new Timestamp(0);

            if (!putDateInTimestamp(log, date)
                    || !putValueFromRegex(log, thread, THREAD_GROUP_ID_IN_PATTERN)
                    || !putValueFromRegex(log, logLvl, LOG_LVL_GROUP_ID_IN_PATTERN)
                    || !putValueFromRegex(log, logMessage, LOG_MESSAGE_GROUP_ID_IN_PATTERN)) {
                continue;
            }

            listCellLog.add(CellLog.builder()
                    .data(date)
                    .thread(thread.toString())
                    .logLvl(logLvl.toString())
                    .logMessage(logMessage.toString())
                    .build());
        }

        return listCellLog;
    }

    private boolean putDateInTimestamp(String s, Timestamp timestamp) {
        try {
            Matcher matcher = normalizeLogPattern.matcher(s);
            matcher.find();
            String dateString = matcher.group(DATE_GROUP_ID_IN_PATTERN);

            dateString = dateString.replace(",", ".");

            timestamp.setTime(convertDateStringToTimestamp(dateString).getTime());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private Timestamp convertDateStringToTimestamp(String s) {
        return Timestamp.valueOf(s);
    }

    private List<String> getValidationLogStrings(StringBuilder nonValidationLog) {
        List<String> validationlog = new LinkedList<>();

        Matcher matcher = normalizeLogPattern.matcher(nonValidationLog);

        while (matcher.find()) {
            validationlog.add(matcher.group());
        }

        return validationlog;
    }

    private boolean putValueFromRegex(String log, StringBuilder text, int idGroup) {
        try {
            Matcher matcher = normalizeLogPattern.matcher(log);
            matcher.find();
            text.delete(0, text.length());
            text.append(matcher.group(idGroup));
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
}
