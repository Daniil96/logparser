package com.staroverov.project.util.library;

import com.staroverov.project.model.CellLog;
import com.staroverov.project.model.LogsRecordReport;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@NoArgsConstructor
@Getter
@Setter
public class WorkWithReportOfLogs {

    public LogsRecordReport toMakeReport(List<CellLog> cellLogs, StringBuilder logsFile){
        LogsRecordReport logsRecordReport = new LogsRecordReport();

        logsRecordReport.setRowsProcessed(calculateRowsProcessed(cellLogs));

        logsRecordReport.setThereWereLines(calculateThereWereLines(logsFile));

        logsRecordReport.setRowsSkipped(calculateRowsSkipped(
                logsRecordReport.getThereWereLines(),
                logsRecordReport.getRowsProcessed()));

        logsRecordReport.setRowsInsertedIntoDatabase(0);


        return logsRecordReport;
    }

    private int calculateThereWereLines(StringBuilder logsFile){
        return logsFile.toString().split("\r\n").length;
    }

    private int calculateRowsProcessed(List<CellLog> cellLogs){
        return cellLogs.size();
    };

    private int calculateRowsSkipped(int itWasLine, int hasBecomeLine){
        return itWasLine-hasBecomeLine;
    };

}
