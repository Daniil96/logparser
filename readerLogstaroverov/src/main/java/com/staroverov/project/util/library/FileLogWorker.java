package com.staroverov.project.util.library;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.io.IOException;

@Component
@NoArgsConstructor
public class FileLogWorker {

    public StringBuilder readLogFile(String pathToFileLog) throws IOException {

        StringBuilder stringBuffer = new StringBuilder();

        FileReader reader = new FileReader(pathToFileLog);

        while (reader.ready()) {

            stringBuffer.append((char) reader.read());
        }
        return stringBuffer;
    }
}
