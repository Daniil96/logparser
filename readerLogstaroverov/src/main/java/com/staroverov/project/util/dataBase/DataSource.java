package com.staroverov.project.util.dataBase;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


@NoArgsConstructor
@Getter
@Setter
public class DataSource{

    private String url;
    private String login;
    private String password ;

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, login, password);
    }

    public void closeConnection(Connection connection) {
        if (connection == null) return;
        try {
            connection.close();
        } catch (SQLException e) {
            System.out.println("Не смог закрыть базу данных");
            e.printStackTrace();
        }
    }
}
