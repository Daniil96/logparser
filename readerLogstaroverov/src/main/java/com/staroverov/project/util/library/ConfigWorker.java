package com.staroverov.project.util.library;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Component
@NoArgsConstructor
public class ConfigWorker {

    private Properties propertyConfig = new Properties();

     public String getValueByKey(String pathFileConfig, String key) throws IOException {

         InputStream inputStream ;
         inputStream = getClass().getClassLoader().getResourceAsStream(pathFileConfig);

         if (inputStream != null) {
             propertyConfig.load(inputStream);
         } else {
             throw new FileNotFoundException("property file '" + pathFileConfig + "' not found in the classpath");
         }

            return propertyConfig.getProperty(key);
    }

     public void setValueByKey(String pathFileConfig, String key, String value) throws IOException {

            FileInputStream fisConfig = new FileInputStream(pathFileConfig);

            propertyConfig.load(fisConfig);
            propertyConfig.setProperty(key, value);
    }



}
