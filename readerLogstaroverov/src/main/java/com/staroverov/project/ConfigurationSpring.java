package com.staroverov.project;

import com.staroverov.project.util.dataBase.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@ComponentScan("com.staroverov.project")
@PropertySource("classpath:jdbc.properties")
public class ConfigurationSpring {

    @Autowired
    private Environment env;

    /*@Value("${jdbc.url}")
    String url;*/

    @Bean
    public DataSource dataSource() {

        DataSource dataSource = new DataSource();
        dataSource.setUrl(env.getProperty("jdbc.url"));
        dataSource.setLogin(env.getProperty("jdbc.login"));
        dataSource.setPassword(env.getProperty("jdbc.password"));

        return dataSource;
    }

    @Bean
    public DriverManagerDataSource dataSourceForJdbcTemplate(){

        DriverManagerDataSource dataSourceForJdbcTemplate;

        dataSourceForJdbcTemplate = new DriverManagerDataSource(
                env.getProperty("jdbc.url"),
                env.getProperty("jdbc.login"),
                env.getProperty("jdbc.password")
        );

        return dataSourceForJdbcTemplate;
    }
}
