CREATE DATABASE logsdb;

DROP TABLE cell_log;

CREATE TABLE cell_log
(
    id SERIAL PRIMARY KEY,
    date VARCHAR(50),
    log_lvl VARCHAR(50),
    thread VARCHAR(50),
    log_Message VARCHAR(2000)

) ;

